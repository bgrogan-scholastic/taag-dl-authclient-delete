##############################################
# Author: Todd A. Reisel
# Module: Apache::grolier::grolier_staticbp.pm
# Purpose: This module works with an existing
#  static page url and transforms it into a
#  cgi request that can then contain includes
# and some other database items if necessary.
##############################################

package Apache::grolier::grolier_staticbp;

use Apache::Const qw(:common);

#use the 1.0 mod_perl compatibility library until the 2.0 api is better implemented.
use Apache::compat();

sub handler {
  #http request variable
  my $requestObject = shift;

  #what is the url the user requested.
  my $uri = $requestObject->uri;

  #what templatename should we use for generating this static "dynamic" page.
  my $templatename = $requestObject->dir_config('STATIC_PAGE_TEMPLATE');

  #what is program we should direct the user to that will invoke the static cgi builder.
  my $staticurl = $ENV{'STATIC_PAGE_URL'};

  #what is the physical file path that the user requested.  
  my $filepath=$requestObject->filename;

  #internally redirect the user to the static build page program with a set of parameters that specifies the templatename and the physical file path.
  $requestObject->internal_redirect($staticurl . "?templatename=" . $templatename . "&pageRequested=" . $filepath);
  
  #tell the server its okay to process this request as a standard 200 code, so proceed.
  return OK;
}

1;

