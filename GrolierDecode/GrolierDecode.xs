#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"


MODULE = GrolierDecode		PACKAGE = GrolierDecode		

void 
grolierEncode(inString)
	char* inString

	CODE:	
		char* outString = NULL;

		 int    i = 0;
		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) + 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';
	
		 strcpy(inString, outString);
		 free(outString);
	OUTPUT:
		 inString



void
grolierDecode(inString)
		char* inString
	
	CODE:
		 char *outString = NULL;
		 int    i = 0;

		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) - 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';

		strcpy(inString, outString);
		free(outString);

	OUTPUT:
		inString
