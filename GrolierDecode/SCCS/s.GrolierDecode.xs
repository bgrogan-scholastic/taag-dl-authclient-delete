h25825
s 00051/00000/00000
d D 1.1 02/11/14 09:26:49 linux 1 0
c tar - 11/14/02 - this code used to be in go_utils.cc in our old codebase (/export/home/...SCCS/go_common) and serves as a perl module with C code to decode/encode old style cookie customer usernames/passwords so that grolier_auth.pm can redirect the user to a new url to upgrade their cookies.
c 
e
u
U
f e 0
t
T
I 1
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"


MODULE = GrolierDecode		PACKAGE = GrolierDecode		

void 
grolierEncode(inString)
	char* inString

	CODE:	
		char* outString = NULL;

		 int    i = 0;
		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) + 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';
	
		 strcpy(inString, outString);
		 free(outString);
	OUTPUT:
		 inString



void
grolierDecode(inString)
		char* inString
	
	CODE:
		 char *outString = NULL;
		 int    i = 0;

		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) - 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';

		strcpy(inString, outString);
		free(outString);

	OUTPUT:
		inString
E 1
