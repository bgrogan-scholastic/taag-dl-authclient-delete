h63382
s 00090/00000/00000
d D 1.1 02/11/14 09:26:48 linux 1 0
c tar - 11/14/02 - this code used to be in go_utils.cc in our old codebase (/export/home/...SCCS/go_common) and serves as a perl module with C code to decode/encode old style cookie customer usernames/passwords so that grolier_auth.pm can redirect the user to a new url to upgrade their cookies.
c 
e
u
U
f e 0
t
T
I 1
/*
 * This file was generated automatically by xsubpp version 1.9508 from the
 * contents of GrolierDecode.xs. Do not edit this file, edit GrolierDecode.xs instead.
 *
 *	ANY CHANGES MADE HERE WILL BE LOST!
 *
 */

#line 1 "GrolierDecode.xs"
#include "EXTERN.h"
#include "perl.h"
#include "XSUB.h"

#include "ppport.h"


#line 18 "GrolierDecode.c"
XS(XS_GrolierDecode_grolierEncode); /* prototype to pass -Wmissing-prototypes */
XS(XS_GrolierDecode_grolierEncode)
{
    dXSARGS;
    if (items != 1)
	Perl_croak(aTHX_ "Usage: GrolierDecode::grolierEncode(inString)");
    {
	char*	inString = (char *)SvPV_nolen(ST(0));
#line 15 "GrolierDecode.xs"
		char* outString = NULL;

		 int    i = 0;
		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) + 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';

		 strcpy(inString, outString);
		 free(outString);
#line 40 "GrolierDecode.c"
	sv_setpv((SV*)ST(0), inString);
	SvSETMAGIC(ST(0));
    }
    XSRETURN_EMPTY;
}

XS(XS_GrolierDecode_grolierDecode); /* prototype to pass -Wmissing-prototypes */
XS(XS_GrolierDecode_grolierDecode)
{
    dXSARGS;
    if (items != 1)
	Perl_croak(aTHX_ "Usage: GrolierDecode::grolierDecode(inString)");
    {
	char*	inString = (char *)SvPV_nolen(ST(0));
#line 37 "GrolierDecode.xs"
		 char *outString = NULL;
		 int    i = 0;

		 outString = (char *) malloc (strlen(inString) +1);
		 while (*(inString +i) != '\0'){
			  *(outString + i) = *(inString + i) - 0x80;
			  i++;
		  }
		 *(outString+i) = '\0';

		strcpy(inString, outString);
		free(outString);

#line 69 "GrolierDecode.c"
	sv_setpv((SV*)ST(0), inString);
	SvSETMAGIC(ST(0));
    }
    XSRETURN_EMPTY;
}

#ifdef __cplusplus
extern "C"
#endif
XS(boot_GrolierDecode); /* prototype to pass -Wmissing-prototypes */
XS(boot_GrolierDecode)
{
    dXSARGS;
    char* file = __FILE__;

    XS_VERSION_BOOTCHECK ;

        newXS("GrolierDecode::grolierEncode", XS_GrolierDecode_grolierEncode, file);
        newXS("GrolierDecode::grolierDecode", XS_GrolierDecode_grolierDecode, file);
    XSRETURN_YES;
}

E 1
