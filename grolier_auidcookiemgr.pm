##############################################
# Author: Todd A. Reisel
# Date: 09/19/02
# Purpose: This class encapsulates the grolier
# authentication auids cookie string and allows
# for the easy retrieval of auid product values.
###############################################

package Apache::grolier::grolier_auidcookiemgr;

  my $GROLIER_AUIDS_COOKIE_VALUE = "";
  my %GROLIER_AUIDS_PRODUCTVALUES = "";

#initialize this structure
sub init {
  #get a copy of the cookie value for auids
  $ac = @_[1];

  $GROLIER_AUIDS_COOKIE_VALUE = $ac;
  #print STDERR "Auids: $GROLIER_AUIDS_COOKIE_VALUE\n";

  #split the individual product key/value pairs and store them in a hash.
  @productList = split(/\|/, $GROLIER_AUIDS_COOKIE_VALUE);

  foreach $productKeyValue (@productList) {
    #print STDERR "$productKeyValue\n";

    @productProperties = split('>' , $productKeyValue);

    #get the productName and the value for this product
    $productName = @productProperties[0];
    $productValue = @productProperties[1];

    #print STDERR "Product Name: $productName:  Product Value: $productValue\n";

    $GROLIER_AUIDS_PRODUCTVALUES{$productName} = $productValue;
  }
}

sub getAuidValue() {
  $productName = @_[1];

  #print STDERR "Looking for product: $productName\n";

  if( $GROLIER_AUIDS_PRODUCTVALUES{$productName} == "") {
    return -1;
  }
  else {
    return $GROLIER_AUIDS_PRODUCTVALUES{$productName};
  }
}

1;

__END__

