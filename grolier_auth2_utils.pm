##############################################
# Author: Todd A. Reisel
# Date: 09/12/02
# Purpose: This class encapsulates the grolier
# authentication cookies needed to validate
# customers and record stats.
###############################################

package Apache2::grolier::grolier_auth2_utils;
use LWP::Simple();
use Apache2::compat();
use URI::Escape;

#load the custom grolier module for encoding / decoding cookies
use GrolierDecode();

use Apache2::grolier::grolier_auidcookiemgr ();
  #authentication cookie keywords.
  my $GROLIER_RAP_COOKIE_NAME = "rap";
  my $GROLIER_AUIDS_COOKIE_NAME = "auids";
  my $GROLIER_AUTH_PASS_COOKIE_NAME = "auth-pass";
  my $GROLIER_AUTH_CURRENTPRODUCT_COOKIE_NAME = "CurrentProduct";
  my $GROLIER_AUTH_PREVIOUSPRODUCT_COOKIE_NAME = "PrevProduct";
  my $GROLIER_AUTH_REFCOOKIE_NAME = "authreferer";

  #authentication variables, to be retreived in init function.
  my %GROLIER_COOKIES_FROM_HTTP_HEADER = "";
  my $GROLIER_AUID_NAME = "";
  my $GROLIER_STATS_FEATURE = "";
  my $GROLIER_CURRENTPRODUCT = "";
  my $GROLIER_PREVIOUSPRODUCT = "";
  my $GROLIER_AUIDS_COOKIE_VALUE = "";
  my $GROLIER_PRODUCT_DOMAIN = "";
  my $GROLIER_STATS_APP = "";
  my $requestObject;


  #environment variables used by authentication.
  my $GROLIER_COOKIE_DOMAIN = $ENV{'GROLIER_COOKIE_DOMAIN'};
  my $GROLIER_AUTH_SERVER = "";
  my $GROLIER_GO_SERVER = "";
  my $GROLIER_AUTH_APP = $ENV{'GROLIER_AUTH_APP'};
  my $GROLIER_AUTH_BUILDFRAMESET = "";
  my $GROLIER_AUTH_INVALIDLOGIN_PAGE = "";

#initialize this structure
sub init {
  #get a copy of the user's request object
  my($self, $r) = @_;
  $requestObject = $r;

  $GROLIER_AUTH_INVALIDLOGIN_PAGE = $requestObject->dir_config("GROLIER_AUTH_INVALIDLOGIN_PAGE");
  $GROLIER_AUTH_BUILDFRAMESET = $requestObject->dir_config("GROLIER_AUTH_BUILDFRAMESET");
  $GROLIER_GO_SERVER = $requestObject->dir_config("GROLIER_GO_SERVER");
  $GROLIER_AUTH_SERVER = $requestObject->dir_config("GROLIER_AUTH_SERVER");

  #get all the cookies sent to us
  $GROLIER_COOKIES_FROM_HTTP_HEADER = $requestObject->header_in('Cookie');
  $GROLIER_AUIDS_COOKIE_VALUE = getCookieValue($GROLIER_COOKIES_FROM_HTTP_HEADER, $GROLIER_AUIDS_COOKIE_NAME);
  $GROLIER_CURRENTPRODUCT = getCookieValue($GROLIER_COOKIES_FROM_HTTP_HEADER,$GROLIER_AUTH_CURRENTPRODUCT_COOKIE_NAME);

  #print STDERR "Current frameset value is: " . $GROLIER_AUTH_BUILDFRAMESET . " and its length is: " . length($GROLIER_AUTH_BUILDFRAMESET) . " for: " . &getRapCookieValue() . "\n";
  #get the frameset parameter

  #get the name of the cookie to look for (ex. nbps-auid or ea-auid, etc).
  $GROLIER_AUID_NAME = $requestObject->dir_config("GROLIER_AUTH_AUIDREQUIRED");

  #what feature should we consider this page for statistical purposes.
  $GROLIER_STATS_FEATURE = $requestObject->dir_config("GROLIER_STATS_FEATURE");

  #get the stats app we should record this hit as.
  $GROLIER_STATS_APP = $requestObject->dir_config('GROLIER_STATS_APP');

  #what product domain are we. (atb.grolier.com, ea.grolier.com, etc. - not machine specific hostname, but product name)
  $GROLIER_PRODUCT_DOMAIN = $requestObject->dir_config('GROLIER_PRODUCT_DOMAIN');
}

#getCookie value
sub getCookieValue() {
  $cookieHeader = @_[0];
  $cookieName = @_[1];

  #get the starting position of the cookie name in the cookie header 
  $startPosition = index($cookieHeader, $cookieName . "=", 0);

  if($startPosition != -1) {

	  #get the ending position of the cookie name in the cookie header by using the delimiting ;.
  	$endPosition = index($cookieHeader, ";", $startPosition +1);

  	#the cookie is at the end of the string because there are no more delimiting ;.
  	if( ($endPosition == -1) && ($startPosition > 0) ){
  		$endPosition = length($cookieHeader);
  	}

  	#find the string length of the cookie name
  	$tagLength = length($cookieName . "=");

  	#get the value of the cookie keyword in the cookie string.
 
  	return substr($GROLIER_COOKIES_FROM_HTTP_HEADER, ($startPosition + $tagLength), ($endPosition - $startPosition - $tagLength));
   }
   else {
	return "";
   }

}

#is the user authenticated to this product.
sub isAuthenticated {
  #print STDERR "Auid name is: " . $GROLIER_AUID_NAME . "\n";
  #print STDERR "Cookie header contains: " . $GROLIER_COOKIES_FROM_HTTP_HEADER . "\n";

  if(index($GROLIER_AUID_NAME, "auid" ) > -1) {
    #we are authenticating against the auids cookie
    #is the auids cookie even present, this would indicate the user has been authenenticated
	
    if( (index($GROLIER_COOKIES_FROM_HTTP_HEADER, ($GROLIER_AUIDS_COOKIE_NAME . "=")) > -1) && (index($GROLIER_COOKIES_FROM_HTTP_HEADER, ($GROLIER_AUIDS_COOKIE_NAME . "=;")) == -1) ){
      
      #is the product specific cookie setting in the auids cookie?
      if(index($GROLIER_COOKIES_FROM_HTTP_HEADER, $GROLIER_AUID_NAME) > -1) {
	#user authenticated to specific product
	return 1;
      }
      else {
	#user is not authenticated to this specific product
	return 2;
      }
    }
    else {
      #user is not authenticated at all!
      return 0;
    }
  }
  else {
    #we are authenticating against the auth-pass cookie being true
    #print STDERR "authenticating by auth-pass=true validation\n";
    if(index($GROLIER_COOKIES_FROM_HTTP_HEADER, $GROLIER_AUTH_PASS_COOKIE_NAME . "=true") > -1) {
      return 1;
    }
    else {
      return 0;
    }
  }
}

sub getAuthValidationUrl() {
  my $authurl = "http://" . $GROLIER_AUTH_SERVER . $GROLIER_AUTH_APP . $GROLIER_AUTH_BUILDFRAMESET;
  #print STDERR $authurl . "\n";
  return $authurl;
}

sub getInvalidLoginPage() {
  #what is the url for the invalid login page.
  return $GROLIER_AUTH_INVALIDLOGIN_PAGE;
}

#return the actual value of the auid cookie being requested, this is useful for statistical purposes.
sub getAuidValue() {

  #create an auid cookie mgr object to deal with parsing the auid cookies
  $myAuthAuidCookieMgr = Apache2::grolier::grolier_auidcookiemgr;
  $myAuthAuidCookieMgr->init( $GROLIER_AUIDS_COOKIE_VALUE);

  #return the auid value (-1 if invalid product)
  #print STDERR "Auid Returned: " .  $myAuthAuidCookieMgr->getAuidValue($GROLIER_AUID_NAME) . "\n";

  return $myAuthAuidCookieMgr->getAuidValue($GROLIER_AUID_NAME);
}

# based on an environment variable to get the app we should record this stat in.
sub getAppValue() {
  return $GROLIER_STATS_APP;
}

#return the environment variable to get the feature we should record this stat in.
sub getFeatureValue() {
	return $GROLIER_STATS_FEATURE;
}

#return the cookie domain used for setting global .grolier.com cookies.
sub getCookieDomain {
  #return the cookie domain used to set cookies for authentication
  return $GROLIER_COOKIE_DOMAIN;
}


#should we update the cookie settings for current/previous product
sub updateCurrentPreviousProduct() {
  #only update this setting if a product domain was specified for the product
  # a good example is gosrch which does not update the product domain as you get a
  # view into the product data.
  #print STDERR "Our Domain is: " . $GROLIER_PRODUCT_DOMAIN . "\n";
  #print STDERR "Current domain is: " . $GROLIER_CURRENTPRODUCT . "\n";

  if( length($GROLIER_PRODUCT_DOMAIN) > 2) {
    if ($GROLIER_CURRENTPRODUCT eq $GROLIER_PRODUCT_DOMAIN) {
      #do nothing
      #print STDERR "not updating current product" . "\n";
    }
    else {
      #send out a cookie header that updates the current product domain.
     my $cookieValue = "$GROLIER_AUTH_CURRENTPRODUCT_COOKIE_NAME=$GROLIER_PRODUCT_DOMAIN; path=/; domain=" . $GROLIER_COOKIE_DOMAIN . ";";
     #print STDERR "New Current Product Cookie is: " . $cookieValue . "\n";

     $requestObject->err_headers_out->add('Set-cookie' => $cookieValue);
    }
  }
}

sub getUserReferer() {
  #get the user's referer
  $referer = $requestObject->header_in('Referer');

  #check to make sure we have a referer
  if( length($referer) > 0) {
    my $cookieValue = "$GROLIER_AUTH_REFCOOKIE_NAME=" . $referer . "; path=/; domain=" . $GROLIER_COOKIE_DOMAIN . ";";
    return $cookieValue;
  }
  else {
    return "";
  }
}

sub getRapCookieValue() {

  $hostname = $requestObject->dir_config("PRODUCT_HOSTNAME");
  $hostport = $requestObject->dir_config("PRODUCT_HOSTPORT");

  my $myUri = Apache2::URI->parse($requestObject, $requestObject->uri);
  $myUri->scheme("http");
  $myUri->hostname($hostname);
  $myUri->port($hostport);
  $myUri->query(scalar $requestObject->args);

  #get the url with servername and port as a string.
  my $uriToRequest = $myUri->unparse();

  #remove the http:// in the front of this string.
  my $strippedHttpUriToRequest = substr($uriToRequest, 7, length($uriToRequest) - 7);

  #build the cookie string based on the uri
  my $cookieValue = "rap=" . $strippedHttpUriToRequest . "; path=/; domain=" . $GROLIER_COOKIE_DOMAIN. ";";
  #print STDERR $cookieValue;

  #return the cookie string
  return $cookieValue;
}

sub processAutoCookieUpgrade() {
	#print STDERR "Grolier Cookies: " . $GROLIER_COOKIES_FROM_HTTP_HEADER . "\n";

	$username = getAutoCookieUpgradeUsername();
	$password = getAutoCookieUpgradePassword();

	#print STDERR "Username is : " . $username . "\n";
	#print STDERR "Password is : " . $password . "\n";

	if ( (length($username) > 1) && (length($password) > 1) ) {
		#print STDERR "Processing Cookie Upgrade Process for : " . $username . " with password: " . $password . "\n";
		return 1;
	}

	return 0;
}

sub getAutoCookieUpgradeUsername() {
	$user = "usr";
	$username = &getCookieValue($GROLIER_COOKIES_FROM_HTTP_HEADER, $user);

	if (length($username) > 0) {
		&GrolierDecode::grolierDecode($username);
		return $username;
	}
	else {
		return "";
	}
}

sub getAutoCookieUpgradePassword() {
	$pass = "pw";
	$password = &getCookieValue($GROLIER_COOKIES_FROM_HTTP_HEADER, $pass);

	if ( length($password) > 0) {
		&GrolierDecode::grolierDecode($password);
		return $password;
	}
	else {
		return "";
	}
}

sub getAutoCookieUpgradeUrl() {
	$upgradeUrl = "http://" . $GROLIER_GO_SERVER . "/cgi-bin/set_cookie?hbpfu=" . getAutoCookieUpgradeUsername() . "&hbpfp=" . getAutoCookieUpgradePassword();
	#print STDERR "The upgrade url is: " . $upgradeUrl . "\n";
	return $upgradeUrl;
}

1;

__END__


