##############################################
# Author: Todd A. Reisel
# Module: Apache::grolier::grolier_stats
# Purpose: this module sits in the logging
# phase of apache and will record hits to the database
#
# Modified: 9/15/2008 by R.E. Dye to support the stats 3.0 schema.
#
##############################################

package Apache::grolier::grolier_stats;

use Apache::Const qw(:common);

use DBI();

use Apache::grolier::grolier_auth_utils ();

sub handler {
  #http request variable
  my $requestObject = shift;

  #create the utilities class with the user's request object
  my $myAuthUtils = Apache::grolier::grolier_auth_utils;
  $myAuthUtils->init($requestObject);

  my $auidValue = $myAuthUtils->getAuidValue();

  #only record statistics when the user is authenticated and has a valid auid cookie, but not the emergency authentication cookie.
  if( ($myAuthUtils->isAuthenticated() == 1)  && ($auidValue > -1) && ($auidValue != 12390) ){

    #get a connection to the database (keep in mind we are using Apache::DBI which has already allocated a connection for us.  The Perl::DBI module already knows of this Apache::DBI module and works with it to get its connection handle which is the same handle if we are in the same child process.  Each apache child process has its own database connection..

    my $databaseHandle = DBI->connect("DBI:mysql:stats:localhost;mysql_socket=/var/lib/mysql/mysql.sock", "stats", "stats");

    #prepare a sql statement for inserting the statistics record.
    # Prepare two sql statements: one for inserting into the remote_activity_log table, and one for the remote_activity_value table
    my $logHandle = $databaseHandle->prepare('insert into remote_activity_log (AU_ID, ACTIVITY_TYPE_ID, ACTIVITY_DATETIME) values ( ?, 1, now())');
    my $valueHandle = $databaseHandle->prepare('insert into remote_activity_value VALUES (LAST_INSERT_ID(), ?, ?)');
    

    my $retval = 0; 	

    #pass along the bind parameters to this sql query
    $retval = $logHandle->execute($myAuthUtils->getAuidValue());

    if ($retval != 1) {
        open(LOGFILE, ">>/data/stats/logs/activity.err") || open(LOGFILE, ">/data/stats/logs/activity.err");
        print LOGFILE $myAuthUtils->getAuidValue() . '|' . $myAuthUtils->getAppValue() . '|' . $myAuthUtils->getFeatureValue() . '|', "\n";
        close LOGFILE;
    }
    else {
	$retval = $valueHandle->execute(0, $myAuthUtils->getAppValue());
	$retval = $valueHandle->execute(1, $myAuthUtils->getFeatureValue());
	# 4/10/2009: R.E. Dye - Add the 'content' service.
	# 5/4/2009: R.E. Dye - changing this to be a PerlSetVar parameter
	$retval = $valueHandle->execute(3, $myAuthUtils->getServiceValue());
    }

    #disconnect from the database
    $databaseHandle->disconnect();

  }

  return OK;
}

1;

__END__

