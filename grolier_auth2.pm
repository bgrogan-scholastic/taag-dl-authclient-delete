##############################################
# Author: Todd A. Reisel
# Module: Apache2::grolier::grolier_auth
# Purpose: this module sits in the authorization/
# authentication phase of apache and will make sure that
# the customers accessing our sites have access to the
# document they are requesting.
#
# All requests will validate that the auid cookie
# for the appropriate server is valid.
##############################################

package Apache2::grolier::grolier_auth2;

use Apache2::Const qw(:common REDIRECT);
use Apache2::compat();

use Apache2::grolier::grolier_auth2_utils ();

#this is how our module gets called, apache knows to call the function handler of this module.
sub handler {
  #http request variable
  my $requestObject = shift;

  #create the utilities class with the user's request object
  my $myAuthUtils = Apache2::grolier::grolier_auth2_utils;
  $myAuthUtils->init($requestObject);

  #is the user authenticated to this product? if so let them in, if not redirect them to our authentication servers.

  #return codes:
  # 1 - user is authenticated to specific product, allow the user access to the document.
  # 2 - user is not authenticated to this product, but has been authenticated, send to invalidlogin page.
  # 0 - user has not passed authentication, send user a redirect to the authentication server.

  if($myAuthUtils->isAuthenticated() == 1) {

    #user is authenticated to the product, do some additional processing.

    #check to see if the CurrentProduct cookie needs to be updated.
    $myAuthUtils->updateCurrentPreviousProduct();

    #user has access
    return OK;
  }
  elsif($myAuthUtils->isAuthenticated() == 0) {
     #user is not authenticated to this product

    #set the rap cookie with the uri they requested access to from the utilities class.
    my $rapValue = $myAuthUtils->getRapCookieValue();
    #print STDERR "Rap cookie value is: " . $rapValue . "\n";

    #send the user a cookie storing the page they requested access to.
    $requestObject->err_headers_out->add('Set-Cookie' => $rapValue);

    #save the user's referer header , so authentication can read it.
    $refererCookieValue = $myAuthUtils->getUserReferer();

    #check to make sure the referer exists.
    if ( length($refererCookieValue) > 0 ) {
      #print STDERR "Referer Cookie Value is: " . $refererCookieValue . "\n";

      #send the user a cookie with the auth referer.
      $requestObject->err_headers_out->add('Set-Cookie' => $refererCookieValue);
    }

    #check to see if we need to upgrade the user's cookies first.
    if( $myAuthUtils->processAutoCookieUpgrade() == 1) {
      $upgradeUrl = $myAuthUtils->getAutoCookieUpgradeUrl();
      $requestObject->header_out('Location' => $upgradeUrl);
      
      return REDIRECT;
    }

    #return a status of REDIRECT, you are not authorized to access this page.
    $requestObject->header_out('Location' => $myAuthUtils->getAuthValidationUrl());
    return REDIRECT;
  }
  elsif($myAuthUtils->isAuthenticated() == 2) {
    #redirect the user to the invalid login page, they have accessed a product to which they are not entitled to.
    $requestObject->content_type("text/html");
    $requestObject->header_out('Location' => $myAuthUtils->getInvalidLoginPage());
    return REDIRECT;
  }
}

1;

__END__


