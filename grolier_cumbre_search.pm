###################################################
# Author: Brian Sanders <bsanders@scholastic.com>
# Module: Apache::grolier::grolier_cumbre_search.pm
# Purpose: This module strips spanish language
# characters out of the search query and redirects
# the request to the gosearch CGI.
###################################################

package Apache::grolier::grolier_cumbre_search;

use Apache::Const qw(:common);
use Apache::compat();

sub handler {

  # Get the Apache request object.
  my $r = shift;

  # Get the search page URL.
  my $url = $ENV{'SEARCH_PAGE_URL'};

  # Get the search paramaters.
  my $args = $r->args;
  my %args = {};
  my (@pairs) = split(/[&;]/, $args);
  foreach my $pair (@pairs) {
    my ($parameter, $value) = split('=', $pair, 2);
    $args{$parameter} = $value;
  }

  # Array of character entities to replace.
  my @patterns = ("%E1", "%E9", "%ED", "%F1", "%F3", "%FA", "%FC", "%C1", "%C9", "%CD", "%D1", "%D3", "%DA", "%DC");

  # Array of replacement characters.
  my @replace = ("a", "e", "i", "n", "o", "u", "u", "A", "E", "I", "N", "0", "U", "U");

  # Strip encoded foreign characters out of the argument list.
  my $count = 0;
  foreach my $pattern(@patterns) {
    $args{'editfield1'} =~ s/($pattern)/$replace[$count]/gi; 
    $args{'editfield2'} =~ s/($pattern)/$replace[$count]/gi; 
    $args{'editfield3'} =~ s/($pattern)/$replace[$count]/gi; 
    $count++;
  } 

  # Check to see which results page template to use (ugh).
  if ($args{'setchoice1'} eq "s") {
    $args{'tn'} = "%2Fsearch%2Fstudent%2Fresults.html";
  } else {
    $args{'tn'} = "%2Fsearch%2Fcumbre%2Fresults.html";
  }

  # Collapse the argument hash back to a query string.
  my $qString = "";
  while (my($key, $val) = each(%args)) {
    if (!($key =~ /HASH/)) {
      $qString .= $key."=".$val."&";
    }
  }
  chop($qString);

  # Create an external redirect to the appropriate search page.
  $r->content_type("text/html");
  $r->header_out('Location' => $url."?".$qString);
  return REDIRECT;
  
}

1;
